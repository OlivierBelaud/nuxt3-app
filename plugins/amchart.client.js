import { defineNuxtPlugin } from '#app'
import * as am5 from "@amcharts/amcharts5";
import * as am5percent from "@amcharts/amcharts5/percent";
import * as am5xy from "@amcharts/amcharts5/xy";

export default defineNuxtPlugin(nuxtApp => {
  nuxtApp.provide('amChart', () => {
    return {
      am5,
      am5percent,
      am5xy
    }
  });
})