import { defineNuxtConfig } from 'nuxt3'

export default defineNuxtConfig({
  srcDir: './',
  vite: {
    // @ts-ignore
    ssr: {
      noExternal: ["@vue/devtools-api"]
    }
  },
  build: {
    transpile: ['algoliasearch/vue3/es'],
    postcss: {
      postcssOptions: {
        plugins: {
          tailwindcss: {},
          autoprefixer: {},
        }
      }
    },
  }
})
