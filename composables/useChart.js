import * as am5 from "@amcharts/amcharts5";
import * as am5pie from "@amcharts/amcharts5/percent";

export default function () {
  return {
    am5,
    am5pie
  }
}
